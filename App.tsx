import { NavigationContainer } from '@react-navigation/native';
import * as Sentry from 'sentry-expo';

import Root from './src/navigation';

Sentry.init({
  dsn: 'https://0631592ff63e4627994336a08718d834@o4504520795095040.ingest.sentry.io/4504520829304832',
  enableInExpoDevelopment: true,
  debug: true, // If `true`, Sentry will try to print out useful debugging information if something goes wrong with sending the event. Set it to `false` in production
});

export default function App() {
  return (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
  );
}
