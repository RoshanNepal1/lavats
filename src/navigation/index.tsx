import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { RootStackParamsList } from './types';
import HomeScreen from '../features/HomeScreen';
import VideoScreen from '../features/VideoScreen';

const Stack = createStackNavigator<RootStackParamsList>();

const Root = () => {
  const homeScreenOptions = {
    headerTitle: 'Home',
  };

  const videoScreenOptions = {
    headerTitle: 'Video',
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'HOME_SCREEN'}
        component={HomeScreen}
        options={homeScreenOptions}
      />
      <Stack.Screen
        name={'VIDEO_SCREEN'}
        component={VideoScreen}
        options={videoScreenOptions}
      />
    </Stack.Navigator>
  );
};

export default Root;
