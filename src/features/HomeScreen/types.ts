import { StackScreenProps } from '@react-navigation/stack';

import { RootStackParamsList } from '../../navigation/types';

export type HomeScreenProps = StackScreenProps<RootStackParamsList, 'HOME_SCREEN'>
