import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, StatusBar} from 'react-native';

import { HomeScreenProps } from './types';

const HomeScreen = ({navigation}: HomeScreenProps) => {

  const _onPressViewVideo = () => navigation.navigate('VIDEO_SCREEN');

  return (
    <View style={styles.container}>

    <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />

    <TouchableOpacity
      style={styles.button}
       onPress={_onPressViewVideo}>
      <Text style={styles.buttonText}>View Video</Text>
    </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  button: {
    height:40,
    borderRadius: 10,
    paddingHorizontal: 12,
    justifyContent: 'center',
    backgroundColor: '#1c1c1c',
  },
  buttonText: {
    color: '#fff'
  }
});
