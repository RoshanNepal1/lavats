import React from 'react';
import YoutubePlayer from 'react-native-youtube-iframe';
import { StyleSheet, View, StatusBar } from 'react-native';

const VideoScreen = () => {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
      <YoutubePlayer play={true} height={300} videoId={'gvkqT_Uoahw'} />
    </View>
  );
};

export default VideoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
