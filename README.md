# Lava

## Run project in development

- Setting up the development environment: https://reactnative.dev/docs/environment-setup.

- Install dependencies: `npm install`.

- Run on Android: `npm run android`.

- Run on iOS: `npm run ios`.

- Run server: `npx expo start`. Then use app **Expo Go** on your mobile and scan QR code on the terminal.
